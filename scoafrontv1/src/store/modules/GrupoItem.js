import axios from 'axios';
//import { async } from 'q';

const state = {
    grupos: []
};

const getters = {
    allGrupos: state => state.grupos.data
}

const actions = {
    async fetchGrupos({ commit }) {
        const response = await axios.get('http://127.0.0.1:8000/api/grupoitems');
        //console.log(response.data);
        commit('setGrupos', response.data);
    },

    async addGrupo({commit}, data){

        var cod_grupo=data.cod_grupo;
        var nom_grupo=data.nom_grupo;
        var observacion= data.observacion;
        var vigencia= data.vigencia;
        const response = await axios.post('http://127.0.0.1:8000/api/grupoitems', {cod_grupo, nom_grupo, observacion, vigencia}
        );
        //console.log(response.data);
        commit('newGrupo', response.data);
    }
};

const mutations = {
    setGrupos: (state, grupos) => state.grupos = grupos,
    newGrupo: (state, data) => state.grupos.data.unshift(data),

};

export default {
    state,
    getters,
    actions,
    mutations
}