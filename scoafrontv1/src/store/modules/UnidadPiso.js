import axios from 'axios';

const state={
    unidadpisos:[]
};

const getters={
    allUnidadPisos:state =>state.unidadpisos.data
};

const actions={
    async fetchUnidadPisos({commit}){
        const response = await axios.get('http://127.0.0.1:8000/api/unidadpisos');
        //console.log(response.data);
        commit('setUnidadPisos', response.data);
    }
}

const mutations={
    setUnidadPisos: (state, unidadpisos)=> state.unidadpisos=unidadpisos

}

export default{
    state,
    getters,
    actions,
    mutations
}