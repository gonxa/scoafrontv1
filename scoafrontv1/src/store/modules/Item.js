import axios from 'axios'

const state = {
    items: []
};

const getters = {
    allItems: state => state.items.data
}

const actions = {
    async fetchItems({ commit }) {
        const response = await axios.get('http://127.0.0.1:8000/api/items');

        commit('setItems', response.data);
    }
};

const mutations = {
    setItems: (state, items) => state.items = items
}

export default {
    state,
    getters,
    actions,
    mutations
}