import axios from 'axios'

const state={
    movimientos:[]
};

const getters={
    allMovimientos:state =>state.movimientos.data
};

const actions={
    async fetchMovimientos({commit}){
        const response = await axios.get('http://127.0.0.1:8000/api/movimientos');
        commit('setMovimientos', response.data);
    }
};

const mutations={
    setMovimientos: (state, movimientos)=>state.movimientos=movimientos
};

export default{
    state,
    getters,
    actions,
    mutations
}