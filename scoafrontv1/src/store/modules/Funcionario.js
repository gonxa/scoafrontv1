import axios from 'axios';

const state = {
    funcionarios:[]
};

const getters ={
    allFuncionarios: state => state.funcionarios.data
};

const actions={
    async fetchFuncionarios({ commit }) {
        const response = await axios.get('http://127.0.0.1:8000/api/usuarios');
        //console.log(response.data);
        commit('setFuncionarios', response.data);
    },
};

const mutations ={
    setFuncionarios: (state, funcionarios) => state.funcionarios = funcionarios,
};

export default {
    state,
    getters,
    actions,
    mutations
}