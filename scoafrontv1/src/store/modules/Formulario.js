import axios from 'axios'

const state ={
    formularios:[]
};

const getters ={
    allFormularios: state => state.formularios.data
}

const actions ={
    async fetchFormularios({commit}){
        const response = await axios.get('http://127.0.0.1:8000/api/formularios');

        commit('setFormularios', response.data);
    }
};

const mutations={
    setFormularios: (state, formularios)=> state.formularios = formularios
}

export default{
    state,
    getters,
    actions,
    mutations
}