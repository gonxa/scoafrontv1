import Vue from 'vue'
import Vuex from 'vuex'
import grupoitems from './store/modules/GrupoItem'
import formularios from './store/modules/Formulario'
import unidadpisos from './store/modules/UnidadPiso'
import movimientos from './store/modules/Movimiento'
import funcionarios from './store/modules/Funcionario'
import items from './store/modules/Item'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    grupoitems, formularios, unidadpisos, movimientos, funcionarios, items
  },
  state: {

  },
  mutations: {

  },
  actions: {

  }
})
