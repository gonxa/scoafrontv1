import Vue from 'vue'
import Router from 'vue-router'
import Dashboard from './views/Dashboard.vue'
import Grupo from './views/GrupoItems.vue'
import Formulario from './views/Formularios.vue'
import NuevoForm from './components/CreateFormulario.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/grupo',
      name: 'grupos',
      component: Grupo
    },
    {
      path: '/',
      name: 'Dashboard',
      component: Dashboard
    },
    {
      path: '/formulario',
      name: 'formularios',
      component: Formulario
    },
    {
      path: '/nuevoform',
      name: 'nuevoform',
      component: NuevoForm
    },
  ]
})
